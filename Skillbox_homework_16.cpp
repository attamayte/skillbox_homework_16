#include <iostream>

// hack to get _current_ day; must rebuild it for every day work
constexpr size_t DAY()
{
    // using advantage of static formatting
    return 10 * (__DATE__[4] - '0') + (__DATE__[5] - '0');
}

int main()
{
    // SIZE - dimension of square array
    // ROW - dynamic matrix index to calc sum of elements
    constexpr size_t SIZE{ 10 }, ROW{ DAY() % SIZE };

    size_t Array[SIZE][SIZE]{}, RowSum{ 0 };

    // printing Array to std::cout
    for (size_t i = 0; i < SIZE; ++i) {
        for (size_t j = 0; j < SIZE; ++j)
            std::cout << (Array[i][j] = i * j) << '\t';
        std::cout << '\n';
    }

    // calculating RowSum
    for (size_t i = 0; i < SIZE; ++i)
        RowSum += Array[ROW][i];
    
    // feedback
    std::cout   << "\nToday is " << __DATE__ << '\n' 
                << "Array size set to " << SIZE << '\n' 
                << "Sum at row " << ROW << " equals " << RowSum << '\n';

    return 0;
}
